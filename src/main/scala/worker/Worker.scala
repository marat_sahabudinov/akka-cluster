package worker

import akka.actor.{Actor, Cancellable, RootActorPath}
import akka.cluster.ClusterEvent.{MemberRemoved, MemberUp}
import akka.cluster.{Cluster, Member}
import com.typesafe.scalalogging.Logger
import master.Master.ReceivedMessagesReport
import worker.Worker._

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration._
import scala.util.Random

object Worker {

  case class SetMessageInterval(interval: Int)

  case class SendMessage()

  case class Message()

  case class ClearOldMessages()

  case class ReportReceivedMessages()

}


class Worker(messageInterval: Int) extends Actor {

  import context.dispatcher

  val logger = Logger(s"Worker $self")

  val cluster = Cluster(context.system)
  var master: Option[Member] = None
  var workers = IndexedSeq[Member]()
  var currentWorker = 0
  var sendSchedule: Option[Cancellable] = None
  var receivedMessageTimestamps = ArrayBuffer[Long]()
  var clearOldMessagesSchedule: Option[Cancellable] = None
  var reportReceivedMessagesSchedule: Option[Cancellable] = None

  override def preStart(): Unit = {
    cluster.subscribe(self, classOf[MemberUp], classOf[MemberRemoved])
    sendSchedule = Some(context.system.scheduler.schedule(Duration.Zero, messageInterval.millis, self, SendMessage))
    clearOldMessagesSchedule = Some(context.system.scheduler.schedule(Duration.Zero, 2000.millis, self, ClearOldMessages))
    reportReceivedMessagesSchedule = Some(context.system.scheduler.schedule(Duration.Zero, 1000.millis, self, ReportReceivedMessages))
  }

  override def postStop(): Unit = {
    sendSchedule.foreach(_.cancel())
    clearOldMessagesSchedule.foreach(_.cancel())
    reportReceivedMessagesSchedule.foreach(_.cancel())
    cluster.unsubscribe(self)
    cluster.leave(cluster.selfAddress)
    context.system.terminate()
  }

  def receive = {
    case SetMessageInterval(interval) =>
      sendSchedule.foreach(_.cancel())
      sendSchedule = Some(context.system.scheduler.schedule(Duration.Zero, interval.millis, self, SendMessage))

    case MemberUp(member) =>
      if (member.hasRole("Master"))
        master = Some(member)
      if (member.hasRole("Worker") && !workers.contains(member))
        workers = workers :+ member

    case MemberRemoved(member, _) if member.hasRole("Worker") =>
      workers = workers.filterNot(_ == member)

    case SendMessage if workers.nonEmpty =>
      currentWorker = (currentWorker + 1) % workers.size
      val member = workers(currentWorker)
      context.actorSelection(RootActorPath(member.address) / "user" / "worker") ! Message

    case Message =>
      receivedMessageTimestamps += System.currentTimeMillis()
      val count = receivedMessageTimestamps.count(_ > System.currentTimeMillis() - 1000)
      logger.info(s"Messages in the last second: $count, actor: $self")

    case ClearOldMessages =>
      receivedMessageTimestamps = receivedMessageTimestamps.filter(_ > System.currentTimeMillis() - 1000)

    case ReportReceivedMessages =>
      val count = receivedMessageTimestamps.count(_ > System.currentTimeMillis() - 1000)
      master.foreach(member => context.actorSelection(RootActorPath(member.address) / "user" / "master") ! ReceivedMessagesReport(count))

  }

}
