package master

import java.nio.file.Paths

import akka.actor.{Actor, ActorSystem, Cancellable, PoisonPill, Props, RootActorPath}
import akka.cluster.ClusterEvent.{MemberRemoved, MemberUp}
import akka.cluster.{Cluster, Member}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.Logger
import master.Master._
import worker.Worker

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration._
import scala.io.StdIn

object Master {

  val logger = Logger(classOf[Master])

  val defaultMessageSendInterval = 100

  def main(args: Array[String]): Unit = {
    val config = ConfigFactory.parseString("akka.cluster.roles=[Master]")
      .withFallback(ConfigFactory.parseFile(Paths.get("node.conf").toFile))
    val system = ActorSystem("ClusterSystem", config)
    val actor = system.actorOf(Props[Master], name = "master")

    // add seed nodes
    actor ! AddNode(port = Some(2551), isSeedNode = true)
    actor ! AddNode(port = Some(2552), isSeedNode = true)

    // REPL
    new Thread(() => {
      val addNode = """add[ ]*([\d]+)""".r
      val removeNode = """rem[ ]*([\d]+)""".r
      val setInterval = """int[ ]*([\d]+)""".r
      val empty = """[ \t]*""".r
      while (true) {
        StdIn.readLine() match {
          case null | empty() =>
          case addNode(number) =>
            logger.info(s"Add $number nodes")
            (1 to number.toInt).foreach(_ => actor ! AddNode(port = None, isSeedNode = false))
          case removeNode(number) =>
            logger.info(s"Remove $number nodes")
            (1 to number.toInt).foreach(_ => actor ! RemoveNode)
          case setInterval(interval) if interval.toInt > 0 =>
            logger.info(s"Set message interval: $interval ms")
            actor ! SetMessageInterval(interval.toInt)
          case x => logger.info(s"Unknown command: $x")
        }
      }
    }).start()
  }

  case class AddNode(port: Option[Int], isSeedNode: Boolean)

  case class RemoveNode()

  case class SetMessageInterval(interval: Int)

  case class ReceivedMessagesReport(messagesInLastSecond: Int)

  case class ClearOldReports()

  case class LogReceivedMessages()

}

class Master extends Actor {

  import context.dispatcher

  val logger = Logger(classOf[Master])

  val cluster = Cluster(context.system)
  var workers = IndexedSeq[Member]()
  // (timestamp, count)
  var receivedReports = ArrayBuffer[(Long, Int)]()
  var clearOldReportsSchedule: Option[Cancellable] = None
  var logReceivedMessagesSchedule: Option[Cancellable] = None
  var messageSendInterval: Int = Master.defaultMessageSendInterval


  override def preStart(): Unit = {
    cluster.subscribe(self, classOf[MemberUp], classOf[MemberRemoved])
    clearOldReportsSchedule = Some(context.system.scheduler.schedule(Duration.Zero, 2000.millis, self, ClearOldReports))
    logReceivedMessagesSchedule = Some(context.system.scheduler.schedule(Duration.Zero, 3000.millis, self, LogReceivedMessages))
  }

  override def postStop(): Unit = {
    clearOldReportsSchedule.foreach(_.cancel())
    logReceivedMessagesSchedule.foreach(_.cancel())
    logReceivedMessagesSchedule.foreach(_.cancel())
    cluster.unsubscribe(self)
  }

  def receive = {
    case AddNode(port, isSeedNode) =>
      logger.info(s"AddNode, port: ${port.getOrElse("random")}, isSeedNode: $isSeedNode")

      val roles = (Seq("Worker") ++ (if (isSeedNode) Seq("Seed") else Seq())).mkString(",")
      val config = ConfigFactory.parseString(s"akka.cluster.roles=[$roles]")
        .withFallback(port
          .map((port) => ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port"))
          .getOrElse(ConfigFactory.empty()))
        .withFallback(ConfigFactory.parseFile(Paths.get("node.conf").toFile))
      val system = ActorSystem("ClusterSystem", config)
      system.actorOf(Props(classOf[Worker], messageSendInterval), name = "worker")

    case RemoveNode =>
      logger.info("RemoveNode")
      workers.find(!_.hasRole("Seed")).foreach(member => {
        workers = workers.filterNot(_ == member)
        context.actorSelection(RootActorPath(member.address) / "user" / "worker") ! PoisonPill
      })

    case SetMessageInterval(interval) =>
      messageSendInterval = interval
      workers.foreach(member => context.actorSelection(RootActorPath(member.address) / "user" / "worker") ! Worker.SetMessageInterval(interval))

    case MemberUp(member) if member.hasRole("Worker") && !workers.contains(member) =>
      logger.info(s"Member up: $member, roles: ${member.roles}")
      workers = workers :+ member

    case MemberRemoved(member, _) if member.hasRole("Worker") =>
      workers = workers.filterNot(_ == member)

    case ReceivedMessagesReport(messagesInLastSecond) =>
      receivedReports += System.currentTimeMillis() -> messagesInLastSecond

    case ClearOldReports =>
      receivedReports = receivedReports.filter(_._1 > System.currentTimeMillis() - 1000)

    case LogReceivedMessages =>
      val count = receivedReports.filter(_._1 > System.currentTimeMillis() - 1000).map(_._2).sum
      val messagesExpected = Math.round(1000.0 / messageSendInterval * workers.size)
      val ratio = Math.round(count / messagesExpected.toDouble * 100).toDouble / 100
      logger.info(s"Total messages in the last second: $count, workers: ${workers.size}, messages expected: $messagesExpected, ratio: $ratio")
  }

}