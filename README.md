## Description
An application which runs multiple actor systems that form a cluster and communicate with each other.

- Every `interval` ms, each node sends a message to some other node, iterating over all nodes in cluster.
- Once message is received, a node logs how many messages it has received in the last second.
- Logs how many message all nodes in the cluster have processed in the last second.
- Provides a console repl interface to add/remove nodes and change message `interval` to find optimal values.

## Architecture
Main components of the system are a master node and a worker node. Two of the worker nodes are used as cluster seed nodes.

## Usage
To run the app, jdk and sbt must be installed.

### Run

	cd to project dir
  	sbt run

Master logs are in console and worker logs are in `workers.log`.

### Console repl commands

	add <count> - add nodes
	rem <count> - remove nodes
	int <interval> - set message interval in milliseconds

## Result
Running multiple actor systems in cluster on a single machine requires a lot of CPU on its own, so it's better to use smaller intervals (2-3 ms) instead of greater number of nodes.

To check efficiency, master prints a ratio of actual to expected throughput:

	Total messages in the last second: 20, workers: 2, messages expected: 20, ratio: 1.0

In the current implementation, there is no handling of down of any node including master node. A simple restart of the node could be enough because every node gets info about all other nodes on start.